package com.ruoyi.web.controller.machine;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.machine.domain.Machine;
import com.ruoyi.machine.service.IMachineService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 设备Controller
 * 
 * @author Jerry
 * @date 2020-06-02
 */
@Controller
@RequestMapping("/machine/machine")
public class MachineController extends BaseController
{
    private String prefix = "machine/machine";

    @Autowired
    private IMachineService machineService;

    @RequiresPermissions("machine:machine:view")
    @GetMapping()
    public String machine()
    {
        return prefix + "/machine";
    }

    /**
     * 查询设备列表
     */
    @RequiresPermissions("machine:machine:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Machine machine)
    {
        startPage();
        List<Machine> list = machineService.selectMachineList(machine);
        return getDataTable(list);
    }

    /**
     * 导出设备列表
     */
    @RequiresPermissions("machine:machine:export")
    @Log(title = "设备", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Machine machine)
    {
        List<Machine> list = machineService.selectMachineList(machine);
        ExcelUtil<Machine> util = new ExcelUtil<Machine>(Machine.class);
        return util.exportExcel(list, "machine");
    }

    /**
     * 新增设备
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存设备
     */
    @RequiresPermissions("machine:machine:add")
    @Log(title = "设备", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Machine machine)
    {
        machine.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(machineService.insertMachine(machine));
    }

    /**
     * 修改设备
     */
    @GetMapping("/edit/{machineId}")
    public String edit(@PathVariable("machineId") Long machineId, ModelMap mmap)
    {
        Machine machine = machineService.selectMachineById(machineId);
        mmap.put("machine", machine);
        return prefix + "/edit";
    }

    /**
     * 修改保存设备
     */
    @RequiresPermissions("machine:machine:edit")
    @Log(title = "设备", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Machine machine)
    {
        return toAjax(machineService.updateMachine(machine));
    }

    /**
     * 删除设备
     */
    @RequiresPermissions("machine:machine:remove")
    @Log(title = "设备", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(machineService.deleteMachineByIds(ids));
    }

    @Log(title = "设备数据导入", businessType = BusinessType.IMPORT)
    @RequiresPermissions("system:user:import")
    @PostMapping("/importData")
    @ResponseBody
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<Machine> util = new ExcelUtil<Machine>(Machine.class);
        List<Machine> userList = util.importExcel(file.getInputStream());

        String operName = ShiroUtils.getSysUser().getLoginName();
        String message = machineService.importMachine(userList, updateSupport, operName);

        return AjaxResult.success(message);
    }

    @RequiresPermissions("system:user:view")
    @GetMapping("/importTemplate")
    @ResponseBody
    public AjaxResult importTemplate()
    {
        ExcelUtil<Machine> util = new ExcelUtil<Machine>(Machine.class);
        return util.importTemplateExcel("设备数据");
    }
}
