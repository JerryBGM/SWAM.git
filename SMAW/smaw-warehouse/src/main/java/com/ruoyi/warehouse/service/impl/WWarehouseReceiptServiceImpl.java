package com.ruoyi.warehouse.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.warehouse.mapper.WWarehouseReceiptMapper;
import com.ruoyi.warehouse.domain.WWarehouseReceipt;
import com.ruoyi.warehouse.service.IWWarehouseReceiptService;
import com.ruoyi.common.core.text.Convert;

/**
 * 仓库类单据Service业务层处理
 * 
 * @author Jerry
 * @date 2020-04-17
 */
@Service
public class WWarehouseReceiptServiceImpl implements IWWarehouseReceiptService 
{
    @Autowired
    private WWarehouseReceiptMapper wWarehouseReceiptMapper;

    /**
     * 查询仓库类单据
     * 
     * @param wReceiptId 仓库类单据ID
     * @return 仓库类单据
     */
    @Override
    public WWarehouseReceipt selectWWarehouseReceiptById(Long wReceiptId)
    {
        return wWarehouseReceiptMapper.selectWWarehouseReceiptById(wReceiptId);
    }

    /**
     * 查询仓库类单据列表
     * 
     * @param wWarehouseReceipt 仓库类单据
     * @return 仓库类单据
     */
    @Override
    public List<WWarehouseReceipt> selectWWarehouseReceiptList(WWarehouseReceipt wWarehouseReceipt)
    {
        return wWarehouseReceiptMapper.selectWWarehouseReceiptList(wWarehouseReceipt);
    }

    /**
     * 新增仓库类单据
     * 
     * @param wWarehouseReceipt 仓库类单据
     * @return 结果
     */
    @Override
    public int insertWWarehouseReceipt(WWarehouseReceipt wWarehouseReceipt)
    {
        return wWarehouseReceiptMapper.insertWWarehouseReceipt(wWarehouseReceipt);
    }

    /**
     * 修改仓库类单据
     * 
     * @param wWarehouseReceipt 仓库类单据
     * @return 结果
     */
    @Override
    public int updateWWarehouseReceipt(WWarehouseReceipt wWarehouseReceipt)
    {
        return wWarehouseReceiptMapper.updateWWarehouseReceipt(wWarehouseReceipt);
    }

    /**
     * 删除仓库类单据对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteWWarehouseReceiptByIds(String ids)
    {
        return wWarehouseReceiptMapper.deleteWWarehouseReceiptByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除仓库类单据信息
     * 
     * @param wReceiptId 仓库类单据ID
     * @return 结果
     */
    @Override
    public int deleteWWarehouseReceiptById(Long wReceiptId)
    {
        return wWarehouseReceiptMapper.deleteWWarehouseReceiptById(wReceiptId);
    }
}
