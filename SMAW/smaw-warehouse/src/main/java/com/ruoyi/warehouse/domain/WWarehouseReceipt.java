package com.ruoyi.warehouse.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 仓库类单据对象 w_warehouse_receipt
 * 
 * @author Jerry
 * @date 2020-04-17
 */
public class WWarehouseReceipt extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 仓库类单据ID */
    private Long wReceiptId;

    /** 单据号 */
    @Excel(name = "单据号")
    private String wReceiptNumber;

    /** 单据类型 */
    @Excel(name = "单据类型")
    private String wReceiptType;

    /** 工单(生产类单据才会有工单) */
    @Excel(name = "工单(生产类单据才会有工单)")
    private String wWorkOrder;

    /** 创建单据日期 */
    @Excel(name = "创建单据日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date wCreateDate;

    /** 补充说明 */
    @Excel(name = "补充说明")
    private String wRemarks;

    /** 创建单据人id */
    @Excel(name = "创建单据人id")
    private Long wCreatePersonId;

    /** 创建人名字 */
    @Excel(name = "创建人名字")
    private String wCreatePersonName;

    /** 物料类型 */
    @Excel(name = "物料类型")
    private String materialType;

    /** 数量 */
    @Excel(name = "数量")
    private Long qty;

    /** 是否审核 */
    @Excel(name = "是否审核")
    private String isCheck;


    public void setwReceiptId(Long wReceiptId) 
    {
        this.wReceiptId = wReceiptId;
    }

    public Long getwReceiptId() 
    {
        return wReceiptId;
    }
    public void setwReceiptNumber(String wReceiptNumber) 
    {
        this.wReceiptNumber = wReceiptNumber;
    }

    public String getwReceiptNumber() 
    {
        return wReceiptNumber;
    }
    public void setwReceiptType(String wReceiptType) 
    {
        this.wReceiptType = wReceiptType;
    }

    public String getwReceiptType() 
    {
        return wReceiptType;
    }
    public void setwWorkOrder(String wWorkOrder) 
    {
        this.wWorkOrder = wWorkOrder;
    }

    public String getwWorkOrder() 
    {
        return wWorkOrder;
    }
    public void setwCreateDate(Date wCreateDate) 
    {
        this.wCreateDate = wCreateDate;
    }

    public Date getwCreateDate() 
    {
        return wCreateDate;
    }
    public void setwRemarks(String wRemarks) 
    {
        this.wRemarks = wRemarks;
    }

    public String getwRemarks() 
    {
        return wRemarks;
    }
    public void setwCreatePersonId(Long wCreatePersonId) 
    {
        this.wCreatePersonId = wCreatePersonId;
    }

    public Long getwCreatePersonId() 
    {
        return wCreatePersonId;
    }
    public void setwCreatePersonName(String wCreatePersonName) 
    {
        this.wCreatePersonName = wCreatePersonName;
    }

    public String getwCreatePersonName() 
    {
        return wCreatePersonName;
    }

    public void setMaterialType(String materialType)
    {
        this.materialType = materialType;
    }

    public String getMaterialType()
    {
        return materialType;
    }
    public void setQty(Long qty)
    {
        this.qty = qty;
    }

    public Long getQty()
    {
        return qty;
    }

    public void setIsCheck(String isCheck)
    {
        this.isCheck = isCheck;
    }

    public String getIsCheck()
    {
        return isCheck;
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("wReceiptId", getwReceiptId())
                .append("wReceiptNumber", getwReceiptNumber())
                .append("wReceiptType", getwReceiptType())
                .append("wWorkOrder", getwWorkOrder())
                .append("wCreateDate", getwCreateDate())
                .append("wRemarks", getwRemarks())
                .append("wCreatePersonId", getwCreatePersonId())
                .append("wCreatePersonName", getwCreatePersonName())
                .append("materialType", getMaterialType())
                .append("qty", getQty())
                .append("isCheck", getIsCheck())
                .toString();
    }
}
