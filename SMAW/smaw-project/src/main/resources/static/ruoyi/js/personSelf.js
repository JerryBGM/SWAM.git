//获取当前时间
function getNowTime() {
        // var myDate = new Date;
        // var year = myDate.getFullYear(); //获取当前年
        // var mon = myDate.getMonth() + 1; //获取当前月
        // var date = myDate.getDate(); //获取当前日
        // var h = myDate.getHours();//获取当前小时数(0-23)
        // var m = myDate.getMinutes();//获取当前分钟数(0-59)
        // var s = myDate.getSeconds();//获取当前秒
        // var week = myDate.getDay();
        // var weeks = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
        // console.log(year, mon, date, weeks[week])
        // var nowTime = year+""+mon+""+date+""+h+""+m+""+s;
        // // $("#time").html(year + "年" + mon + "月" + date + "日" + weeks[week]);
        // console.log(nowTime);

        var nowDate = new Date();
        var year = nowDate.getFullYear();
        var month = nowDate.getMonth() + 1 < 10 ? "0" + (nowDate.getMonth() + 1) : nowDate.getMonth() + 1;
        var date = nowDate.getDate() < 10 ? "0" + nowDate.getDate() : nowDate.getDate();
        var hour = nowDate.getHours()< 10 ? "0" + nowDate.getHours() : nowDate.getHours();
        var minute = nowDate.getMinutes()< 10 ? "0" + nowDate.getMinutes() : nowDate.getMinutes();
        var second = nowDate.getSeconds()< 10 ? "0" + nowDate.getSeconds() : nowDate.getSeconds();
        // var week = myDate.getDay();
        // var weeks = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
        var nowTime =year + "" + month + "" + date+""+hour+""+minute+""+second;//year + "-" + month + "-" + date+" "+hour+":"+minute+":"+second;
        return nowTime;
    };

//生成0-9，A-Z随机数（去掉了O，I字母）
function RandomRange(min, max) {
    var returnStr = "",
        range = (max ? Math.round(Math.random() * (max-min)) + min : min),
        arr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    for(var i=0; i<range; i++){
        var index = Math.round(Math.random() * (arr.length-1));
        returnStr += arr[index];
    }
    return returnStr;
};
//生成工单
function  createWorkOrder() {
    var nowDate = getNowTime();
    var random = RandomRange(4,4);
    return "WORK-"+nowDate;
};
//仓库单据
function createOrder(typeListText) {
    var nowDate = getNowTime();
    var random = RandomRange(4,4);
    var order,begin;
    switch (typeListText) {
        case "生产领料单":
            begin = "RI";
            break;
        case "发货出库单":
            begin = "SO";
            break;
        case "生产出库单":
            begin = "PWO";
            break;
        case "生产入库单":
            begin = "PWI";
            break;
        case "退料入库单":
            begin = "TI";
            break;
        case "废料入库单":
            begin = "SI";
            break;
        case "退货入库单":
            begin = "RTI";
            break;
    }
    order = begin+""+nowDate+""+random
    return order;
};
