package com.ruoyi.warehouse.service;

import com.ruoyi.warehouse.domain.WWareHouse;
import java.util.List;

/**
 * 仓库信息主表Service接口
 * 
 * @author Jerry
 * @date 2020-04-13
 */
public interface IWWareHouseService 
{
    /**
     * 查询仓库信息主表
     * 
     * @param wareHouseId 仓库信息主表ID
     * @return 仓库信息主表
     */
    public WWareHouse selectWWareHouseById(Long wareHouseId);

    /**
     * 查询仓库信息主表列表
     * 
     * @param wWareHouse 仓库信息主表
     * @return 仓库信息主表集合
     */
    public List<WWareHouse> selectWWareHouseList(WWareHouse wWareHouse);

    /**
     * 新增仓库信息主表
     * 
     * @param wWareHouse 仓库信息主表
     * @return 结果
     */
    public int insertWWareHouse(WWareHouse wWareHouse);

    /**
     * 修改仓库信息主表
     * 
     * @param wWareHouse 仓库信息主表
     * @return 结果
     */
    public int updateWWareHouse(WWareHouse wWareHouse);

    /**
     * 批量删除仓库信息主表
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWWareHouseByIds(String ids);

    /**
     * 删除仓库信息主表信息
     * 
     * @param wareHouseId 仓库信息主表ID
     * @return 结果
     */
    public int deleteWWareHouseById(Long wareHouseId);
}
