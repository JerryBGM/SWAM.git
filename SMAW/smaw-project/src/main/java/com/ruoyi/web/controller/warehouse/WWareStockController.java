package com.ruoyi.web.controller.warehouse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.warehouse.domain.WWareHouse;
import com.ruoyi.warehouse.domain.WWareStock;
import com.ruoyi.warehouse.service.IWWareHouseService;
import com.ruoyi.warehouse.service.IWWareStockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 库存Controller
 *
 * @author Jerry
 * @date 2020-04-14
 */
@Api("库存详情")
@Controller
@RequestMapping("/warehouse/stock")
public class WWareStockController extends BaseController
{
    private String prefix = "warehouse/stock";

    @Autowired
    private IWWareStockService wWareStockService;
    @Autowired
    private IWWareHouseService wWareHouseService;

    @RequiresPermissions("warehouse:stock:view")
    @GetMapping()
    public String stock()
    {
        return prefix + "/stock";
    }

    @RequiresPermissions("warehouse:stock:view")
    @GetMapping("/toview")
    public String stocks(Long stockHouseId,ModelMap mmap)
    {
        mmap.put("stockHouseId",stockHouseId);
        return prefix + "/stock";
    }
    /**
     * 查询库存列表
     */
    @ApiOperation("查询库存列表")
    @RequiresPermissions("warehouse:stock:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(WWareStock wWareStock)
    {
        startPage();
        List<WWareStock> list = wWareStockService.selectWWareStockList(wWareStock);
        return getDataTable(list);
    }

    /**
     * 导出库存列表
     */
    @RequiresPermissions("warehouse:stock:export")
    @Log(title = "库存", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(WWareStock wWareStock)
    {
        List<WWareStock> list = wWareStockService.selectWWareStockList(wWareStock);
        ExcelUtil<WWareStock> util = new ExcelUtil<WWareStock>(WWareStock.class);
        return util.exportExcel(list, "stock");
    }

    /**
     * 新增库存
     */
    @GetMapping("/add")
    public String add(WWareHouse wWareHouse,ModelMap mmap)
    {
        List<WWareHouse> list = wWareHouseService.selectWWareHouseList(wWareHouse);
        mmap.put("warehouse",list);
        return prefix + "/add";
    }

    /**
     * 新增保存库存
     */
    @RequiresPermissions("warehouse:stock:add")
    @Log(title = "库存", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(WWareStock wWareStock)
    {
        wWareStock.setInStockPerson(ShiroUtils.getLoginName());
        return toAjax(wWareStockService.insertWWareStock(wWareStock));
    }

    /**
     * 修改库存
     */
    @GetMapping("/edit/{stockId}")
    public String edit(@PathVariable("stockId") Long stockId, ModelMap mmap,WWareHouse wWareHouse)
    {
        List<WWareHouse> list = wWareHouseService.selectWWareHouseList(wWareHouse);
        mmap.put("warehouse",list);
        WWareStock wWareStock = wWareStockService.selectWWareStockById(stockId);
        mmap.put("wWareStock", wWareStock);
        return prefix + "/edit";
    }

    /**
     * 修改保存库存
     */
    @RequiresPermissions("warehouse:stock:edit")
    @Log(title = "库存", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(WWareStock wWareStock)
    {
        return toAjax(wWareStockService.updateWWareStock(wWareStock));
    }

    /**
     * 删除库存
     */
    @RequiresPermissions("warehouse:stock:remove")
    @Log(title = "库存", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(wWareStockService.deleteWWareStockByIds(ids));
    }

    /**
     * 查询库存
     */
    @RequiresPermissions("warehouse:stock:query")
    @PostMapping( "/query")
    @ResponseBody
    public TableDataInfo queryWWareStockBystockHouseId(WWareStock wWareStock,Long stockHouseId)
    {
        List<WWareStock> list = wWareStockService.selectWWareStockList(wWareStock);
        return getDataTable(list);
    }

}
