package com.ruoyi.web.controller.bom;

import com.ruoyi.bom.service.IBomDetailService;
import com.ruoyi.bom.service.IBomService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.BomDetail;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * bom明细Controller
 *
 * @author Jerry
 * @date 2020-05-27
 */
@Controller
@RequestMapping("/bom/detail")
public class BomDetailController extends BaseController
{
    private String prefix = "bom/detail";

    @Autowired
    private IBomDetailService bomDetailService;
    @Autowired
    private IBomService bomService;

    @RequiresPermissions("bom:detail:view")
    @GetMapping()
    public String detail()
    {
        return prefix + "/detail";
    }

    /**
     * 查询bom明细列表
     */
    @RequiresPermissions("bom:detail:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BomDetail bomDetail)
    {
        startPage();
        List<BomDetail> list = bomDetailService.selectBomDetailList(bomDetail);
        return getDataTable(list);
    }

    /**
     * 导出bom明细列表
     */
    @RequiresPermissions("bom:detail:export")
    @Log(title = "bom明细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BomDetail bomDetail)
    {
        List<BomDetail> list = bomDetailService.selectBomDetailList(bomDetail);
        ExcelUtil<BomDetail> util = new ExcelUtil<BomDetail>(BomDetail.class);
        return util.exportExcel(list, "detail");
    }

    /**
     * 新增bom明细
     */
    @GetMapping("/add/{bomName}")
    public String add(@PathVariable("bomName") String bomName, ModelMap mmap)
    {
        mmap.put("bomName",bomName);
        return prefix + "/add";
    }

    /**
     * 新增保存bom明细
     */
    @RequiresPermissions("bom:detail:add")
    @Log(title = "bom明细", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BomDetail bomDetail)
    {
        return toAjax(bomDetailService.insertBomDetail(bomDetail));
    }

    /**
     * 修改bom明细
     */
    @GetMapping("/edit/{bomDetailId}")
    public String edit(@PathVariable("bomDetailId") Long bomDetailId, ModelMap mmap)
    {
        BomDetail bomDetail = bomDetailService.selectBomDetailById(bomDetailId);
        mmap.put("bomDetail", bomDetail);
        return prefix + "/edit";
    }

    /**
     * 修改保存bom明细
     */
    @RequiresPermissions("bom:detail:edit")
    @Log(title = "bom明细", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BomDetail bomDetail)
    {
        return toAjax(bomDetailService.updateBomDetail(bomDetail));
    }

    /**
     * 删除bom明细
     */
    @RequiresPermissions("bom:detail:remove")
    @Log(title = "bom明细", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(bomDetailService.deleteBomDetailByIds(ids));
    }

    /**
     * 查询字典详细
     */
    @RequiresPermissions("bom:detail:list")
    @GetMapping("/detail/{dictId}")
    public String detail(@PathVariable("dictId") Long dictId,ModelMap map)
    {
        map.put("BOMID",bomService.selectBomById(dictId));
        map.put("BOMList",bomService.selectAll());
        return "bom/detail/detail";
    }
}