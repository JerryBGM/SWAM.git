package com.ruoyi.workorder.service;

import com.ruoyi.workorder.domain.WorkOrderDetail;
import java.util.List;

/**
 * 工单明细Service接口
 * 
 * @author Jerry
 * @date 2020-06-04
 */
public interface IWorkOrderDetailService 
{
    /**
     * 查询工单明细
     * 
     * @param detailId 工单明细ID
     * @return 工单明细
     */
    public WorkOrderDetail selectWorkOrderDetailById(Long detailId);

    /**
     * 查询工单明细列表
     * 
     * @param workOrderDetail 工单明细
     * @return 工单明细集合
     */
    public List<WorkOrderDetail> selectWorkOrderDetailList(WorkOrderDetail workOrderDetail);

    /**
     * 新增工单明细
     * 
     * @param workOrderDetail 工单明细
     * @return 结果
     */
    public int insertWorkOrderDetail(WorkOrderDetail workOrderDetail);

    /**
     * 修改工单明细
     * 
     * @param workOrderDetail 工单明细
     * @return 结果
     */
    public int updateWorkOrderDetail(WorkOrderDetail workOrderDetail);

    /**
     * 批量删除工单明细
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWorkOrderDetailByIds(String ids);

    /**
     * 删除工单明细信息
     * 
     * @param detailId 工单明细ID
     * @return 结果
     */
    public int deleteWorkOrderDetailById(Long detailId);
}
