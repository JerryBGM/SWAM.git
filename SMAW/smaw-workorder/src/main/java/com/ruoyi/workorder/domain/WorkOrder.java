package com.ruoyi.workorder.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 生产工单对象 work_order
 * 
 * @author Jerry
 * @date 2020-06-03
 */
public class WorkOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 工单号 */
    @Excel(name = "工单号")
    private String workOrder;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createPerson;

    /** 计划产能 */
    @Excel(name = "计划产能")
    private Long planQty;

    /** 实际产能 */
    @Excel(name = "实际产能")
    private Long realQty;

    /** 负责人id */
    @Excel(name = "负责人id")
    private Long principalId;

    /** 负责人名字 */
    @Excel(name = "负责人名字")
    private String principalName;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setWorkOrder(String workOrder) 
    {
        this.workOrder = workOrder;
    }

    public String getWorkOrder() 
    {
        return workOrder;
    }
    public void setCreatePerson(String createPerson) 
    {
        this.createPerson = createPerson;
    }

    public String getCreatePerson() 
    {
        return createPerson;
    }
    public void setPlanQty(Long planQty) 
    {
        this.planQty = planQty;
    }

    public Long getPlanQty() 
    {
        return planQty;
    }
    public void setRealQty(Long realQty) 
    {
        this.realQty = realQty;
    }

    public Long getRealQty() 
    {
        return realQty;
    }
    public void setPrincipalId(Long principalId) 
    {
        this.principalId = principalId;
    }

    public Long getPrincipalId() 
    {
        return principalId;
    }
    public void setPrincipalName(String principalName) 
    {
        this.principalName = principalName;
    }

    public String getPrincipalName() 
    {
        return principalName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("workOrder", getWorkOrder())
            .append("createTime", getCreateTime())
            .append("createPerson", getCreatePerson())
            .append("planQty", getPlanQty())
            .append("realQty", getRealQty())
            .append("principalId", getPrincipalId())
            .append("principalName", getPrincipalName())
            .toString();
    }
}
