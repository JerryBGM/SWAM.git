package com.ruoyi.warehouse.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 库存对象 w_ware_stock
 * 
 * @author Jerry
 * @date 2020-04-14
 */
public class WWareStock extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 库存id */
    private Long stockId;

    /** 入库单号 */
    @Excel(name = "入库单号")
    private String inStockNumber;

    /** 入库日期 */
    @Excel(name = "入库日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date inStockDate;

    /** 入库人 */
    @Excel(name = "入库人")
    private String inStockPerson;

    /** 数量 */
    @Excel(name = "数量")
    private Long stockQty;

    /** 所在仓库ID */
    @Excel(name = "所在仓库ID")
    private Long stockHouseId;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String stockHouseName;

    /** 物料属性 */
    @Excel(name = "物料属性")
    private String materialProperties;

    /** 物料名称 */
    @Excel(name = "物料名称")
    private String materialName;

    public void setStockId(Long stockId) 
    {
        this.stockId = stockId;
    }

    public Long getStockId() 
    {
        return stockId;
    }
    public void setInStockNumber(String inStockNumber) 
    {
        this.inStockNumber = inStockNumber;
    }

    public String getInStockNumber() 
    {
        return inStockNumber;
    }
    public void setInStockDate(Date inStockDate) 
    {
        this.inStockDate = inStockDate;
    }

    public Date getInStockDate() 
    {
        return inStockDate;
    }
    public void setInStockPerson(String inStockPerson) 
    {
        this.inStockPerson = inStockPerson;
    }

    public String getInStockPerson() 
    {
        return inStockPerson;
    }
    public void setStockQty(Long stockQty) 
    {
        this.stockQty = stockQty;
    }

    public Long getStockQty() 
    {
        return stockQty;
    }
    public void setStockHouseId(Long stockHouseId) 
    {
        this.stockHouseId = stockHouseId;
    }

    public Long getStockHouseId() 
    {
        return stockHouseId;
    }
    public void setStockHouseName(String stockHouseName) 
    {
        this.stockHouseName = stockHouseName;
    }

    public String getStockHouseName() 
    {
        return stockHouseName;
    }

    public void setMaterialProperties(String materialProperties)
    {
        this.materialProperties = materialProperties;
    }

    public String getMaterialProperties()
    {
        return materialProperties;
    }
    public void setMaterialName(String materialName)
    {
        this.materialName = materialName;
    }

    public String getMaterialName()
    {
        return materialName;
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("stockId", getStockId())
                .append("inStockNumber", getInStockNumber())
                .append("inStockDate", getInStockDate())
                .append("inStockPerson", getInStockPerson())
                .append("stockQty", getStockQty())
                .append("stockHouseId", getStockHouseId())
                .append("stockHouseName", getStockHouseName())
                .append("materialProperties", getMaterialProperties())
                .append("materialName", getMaterialName())
                .toString();
    }
}
