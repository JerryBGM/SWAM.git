package com.ruoyi.machine.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.machine.domain.Machine;
import com.ruoyi.machine.mapper.MachineMapper;
import com.ruoyi.machine.service.IMachineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 设备Service业务层处理
 * 
 * @author Jerry
 * @date 2020-06-02
 */
@Service
public class MachineServiceImpl implements IMachineService 
{
    private static final Logger log = LoggerFactory.getLogger(MachineServiceImpl.class);

    @Autowired
    private MachineMapper machineMapper;


    /**
     * 查询设备
     * 
     * @param machineId 设备ID
     * @return 设备
     */
    @Override
    public Machine selectMachineById(Long machineId)
    {
        return machineMapper.selectMachineById(machineId);
    }

    /**
     * 查询设备列表
     * 
     * @param machine 设备
     * @return 设备
     */
    @Override
    public List<Machine> selectMachineList(Machine machine)
    {
        return machineMapper.selectMachineList(machine);
    }

    /**
     * 新增设备
     * 
     * @param machine 设备
     * @return 结果
     */
    @Override
    public int insertMachine(Machine machine)
    {
        machine.setCreateTime(DateUtils.getNowDate());
        return machineMapper.insertMachine(machine);
    }

    /**
     * 修改设备
     * 
     * @param machine 设备
     * @return 结果
     */
    @Override
    public int updateMachine(Machine machine)
    {
        machine.setUpdateTime(DateUtils.getNowDate());
        return machineMapper.updateMachine(machine);
    }

    /**
     * 删除设备对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMachineByIds(String ids)
    {
        return machineMapper.deleteMachineByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除设备信息
     * 
     * @param machineId 设备ID
     * @return 结果
     */
    @Override
    public int deleteMachineById(Long machineId)
    {
        return machineMapper.deleteMachineById(machineId);
    }

    @Override
    public String importMachine(List<Machine> machineList, Boolean isUpdateSupport, String operName) {
        if (StringUtils.isNull(machineList) || machineList.size() == 0)
        {
            throw new BusinessException("导入数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (Machine m : machineList){
            try {
                Machine machine = new Machine();
                machine.setMachineNumber(m.getMachineNumber());
                List<Machine> machines = machineMapper.selectMachineList(machine);
                if(machines.size()>0){
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、设备 " + m.getMachineNumber()+ " 已存在");
                }
                m.setCreateBy(operName);
                this.insertMachine(m);
                successNum++;
                successMsg.append("<br/>" + successNum + "、账号 " + m.getMachineNumber() + " 导入成功");
            }catch (Exception e){
                String msg = "";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
            if (failureNum > 0)
            {
                failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
                throw new BusinessException(failureMsg.toString());
            }
            else
            {
                successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
            }
        }
        return successMsg.toString();
    }
//    /**
//     * 导入用户数据
//     *
//     * @param userList 用户数据列表
//     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
//     * @param operName 操作用户
//     * @return 结果
//     */
//    @Override
//    public String importUser(List<SysUser> userList, Boolean isUpdateSupport, String operName)
//    {
//        if (StringUtils.isNull(userList) || userList.size() == 0)
//        {
//            throw new BusinessException("导入用户数据不能为空！");
//        }
//        int successNum = 0;
//        int failureNum = 0;
//        StringBuilder successMsg = new StringBuilder();
//        StringBuilder failureMsg = new StringBuilder();
//        String password = configService.selectConfigByKey("sys.user.initPassword");
//        for (SysUser user : userList)
//        {
//            try
//            {
//                // 验证是否存在这个用户
//                SysUser u = userMapper.selectUserByLoginName(user.getLoginName());
//                if (StringUtils.isNull(u))
//                {
//                    user.setPassword(Md5Utils.hash(user.getLoginName() + password));
//                    user.setCreateBy(operName);
//                    this.insertUser(user);
//                    successNum++;
//                    successMsg.append("<br/>" + successNum + "、账号 " + user.getLoginName() + " 导入成功");
//                }
//                else if (isUpdateSupport)
//                {
//                    user.setUpdateBy(operName);
//                    this.updateUser(user);
//                    successNum++;
//                    successMsg.append("<br/>" + successNum + "、账号 " + user.getLoginName() + " 更新成功");
//                }
//                else
//                {
//                    failureNum++;
//                    failureMsg.append("<br/>" + failureNum + "、账号 " + user.getLoginName() + " 已存在");
//                }
//            }
//            catch (Exception e)
//            {
//                failureNum++;
//                String msg = "<br/>" + failureNum + "、账号 " + user.getLoginName() + " 导入失败：";
//                failureMsg.append(msg + e.getMessage());
//                log.error(msg, e);
//            }
//        }
//        if (failureNum > 0)
//        {
//            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
//            throw new BusinessException(failureMsg.toString());
//        }
//        else
//        {
//            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
//        }
//        return successMsg.toString();
//    }
}
