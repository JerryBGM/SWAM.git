package com.ruoyi.machine.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 设备对象 machine
 * 
 * @author Jerry
 * @date 2020-06-02
 */
public class Machine extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 设备id */
    private Long machineId;

    /** 设备编码 */
    @Excel(name = "设备编码")
    private String machineNumber;

    /** 设备名称 */
    @Excel(name = "设备名称")
    private String machineName;

    /** 设备状态 0:闲置 1:使用 */
    @Excel(name = "设备状态 0:闲置 1:使用")
    private String machineStatus;

    /** 设备位置 */
    @Excel(name = "设备位置")
    private String location;

    /** 负责人 */
    @Excel(name = "负责人")
    private String principal;

    public void setMachineId(Long machineId) 
    {
        this.machineId = machineId;
    }

    public Long getMachineId() 
    {
        return machineId;
    }
    public void setMachineNumber(String machineNumber) 
    {
        this.machineNumber = machineNumber;
    }

    public String getMachineNumber() 
    {
        return machineNumber;
    }
    public void setMachineName(String machineName) 
    {
        this.machineName = machineName;
    }

    public String getMachineName() 
    {
        return machineName;
    }
    public void setMachineStatus(String machineStatus) 
    {
        this.machineStatus = machineStatus;
    }

    public String getMachineStatus() 
    {
        return machineStatus;
    }
    public void setLocation(String location) 
    {
        this.location = location;
    }

    public String getLocation() 
    {
        return location;
    }
    public void setPrincipal(String principal) 
    {
        this.principal = principal;
    }

    public String getPrincipal() 
    {
        return principal;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("machineId", getMachineId())
            .append("machineNumber", getMachineNumber())
            .append("machineName", getMachineName())
            .append("machineStatus", getMachineStatus())
            .append("location", getLocation())
            .append("principal", getPrincipal())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
