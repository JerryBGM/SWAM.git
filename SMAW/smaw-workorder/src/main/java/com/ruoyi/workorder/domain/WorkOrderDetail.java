package com.ruoyi.workorder.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 工单明细对象 work_order_detail
 * 
 * @author Jerry
 * @date 2020-06-04
 */
public class WorkOrderDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 工单明细表 */
    private Long detailId;

    /** 工单主表id */
    @Excel(name = "工单主表id")
    private Long workOrderId;

    /** 设备id */
    @Excel(name = "设备id")
    private Long machineId;

    /** bom Id */
    @Excel(name = "bom Id")
    private Long bomId;

    /** 工序工单号 */
    @Excel(name = "工序工单号")
    private String processWorkOrder;

    /** 工序 1:初始工序 2：加工工序 3：末道工序 */
    @Excel(name = "工序 1:初始工序 2：加工工序 3：末道工序")
    private String process;

    /** 状态 1:新建 2:开工 3:暂停  4：故障 5:完工 6:作废 */
    @Excel(name = "状态 1:新建 2:开工 3:暂停  4：故障 5:完工 6:作废")
    private String status;

    /** 工序工单号 */
    @Excel(name = "工序工单号")
    private String workOrder;

    /** 设备名称 */
    @Excel(name = "设备名称")
    private String machineName;

    /** bom名 */
    @Excel(name = "bom名")
    private String bomName;


    public void setDetailId(Long detailId) 
    {
        this.detailId = detailId;
    }

    public Long getDetailId() 
    {
        return detailId;
    }
    public void setWorkOrderId(Long workOrderId) 
    {
        this.workOrderId = workOrderId;
    }

    public Long getWorkOrderId() 
    {
        return workOrderId;
    }
    public void setMachineId(Long machineId) 
    {
        this.machineId = machineId;
    }

    public Long getMachineId() 
    {
        return machineId;
    }
    public void setBomId(Long bomId) 
    {
        this.bomId = bomId;
    }

    public Long getBomId() 
    {
        return bomId;
    }
    public void setProcess(String process) 
    {
        this.process = process;
    }

    public String getProcess() 
    {
        return process;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    public void setProcessWorkOrder(String processWorkOrder)
    {
        this.processWorkOrder = processWorkOrder;
    }

    public String getProcessWorkOrder()
    {
        return processWorkOrder;
    }

    public String getWorkOrder() {
        return workOrder;
    }

    public void setWorkOrder(String workOrder) {
        this.workOrder = workOrder;
    }

    public String getMachineName() {
        return machineName;
    }

    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    public String getBomName() {
        return bomName;
    }

    public void setBomName(String bomName) {
        this.bomName = bomName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("detailId", getDetailId())
            .append("workOrderId", getWorkOrderId())
            .append("machineId", getMachineId())
            .append("bomId", getBomId())
            .append("processWorkOrder", getProcessWorkOrder())
            .append("process", getProcess())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("workOrder", getWorkOrder())
            .append("machineName", getMachineName())
            .append("bomName", getBomName())
            .toString();
    }
}
