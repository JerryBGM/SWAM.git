package com.ruoyi.warehouse.service;

import com.ruoyi.warehouse.domain.WWareStock;
import java.util.List;

/**
 * 库存Service接口
 * 
 * @author Jerry
 * @date 2020-04-14
 */
public interface IWWareStockService 
{
    /**
     * 查询库存
     * 
     * @param stockId 库存ID
     * @return 库存
     */
    public WWareStock selectWWareStockById(Long stockId);

    /**
     * 查询库存列表
     * 
     * @param wWareStock 库存
     * @return 库存集合
     */
    public List<WWareStock> selectWWareStockList(WWareStock wWareStock);

    /**
     * 新增库存
     * 
     * @param wWareStock 库存
     * @return 结果
     */
    public int insertWWareStock(WWareStock wWareStock);

    /**
     * 修改库存
     * 
     * @param wWareStock 库存
     * @return 结果
     */
    public int updateWWareStock(WWareStock wWareStock);

    /**
     * 批量删除库存
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWWareStockByIds(String ids);

    /**
     * 删除库存信息
     * 
     * @param stockId 库存ID
     * @return 结果
     */
    public int deleteWWareStockById(Long stockId);

    /**
     *查找库存对象
     *
     * @param stockHouseId 仓库id
     * @return 结果
     */
    public int queryWWareStockBystockHouseId(Long stockHouseId);
}
