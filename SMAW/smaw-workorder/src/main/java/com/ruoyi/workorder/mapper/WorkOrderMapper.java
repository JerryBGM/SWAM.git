package com.ruoyi.workorder.mapper;

import com.ruoyi.workorder.domain.WorkOrder;
import java.util.List;

/**
 * 生产工单Mapper接口
 * 
 * @author Jerry
 * @date 2020-06-03
 */
public interface WorkOrderMapper 
{
    /**
     * 查询生产工单
     * 
     * @param id 生产工单ID
     * @return 生产工单
     */
    public WorkOrder selectWorkOrderById(Long id);

    /**
     * 查询生产工单列表
     * 
     * @param workOrder 生产工单
     * @return 生产工单集合
     */
    public List<WorkOrder> selectWorkOrderList(WorkOrder workOrder);

    /**
     * 新增生产工单
     * 
     * @param workOrder 生产工单
     * @return 结果
     */
    public int insertWorkOrder(WorkOrder workOrder);

    /**
     * 修改生产工单
     * 
     * @param workOrder 生产工单
     * @return 结果
     */
    public int updateWorkOrder(WorkOrder workOrder);

    /**
     * 删除生产工单
     * 
     * @param id 生产工单ID
     * @return 结果
     */
    public int deleteWorkOrderById(Long id);

    /**
     * 批量删除生产工单
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWorkOrderByIds(String[] ids);
}
