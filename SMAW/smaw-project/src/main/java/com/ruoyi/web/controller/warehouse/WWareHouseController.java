package com.ruoyi.web.controller.warehouse;

import java.util.List;

import com.ruoyi.framework.util.ShiroUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.warehouse.domain.WWareHouse;
import com.ruoyi.warehouse.service.IWWareHouseService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 仓库信息主表Controller
 * 
 * @author Jerry
 * @date 2020-04-13
 */
@Api("仓库信息主表")
@Controller
@RequestMapping("/warehouse/warehouse")
public class WWareHouseController extends BaseController
{
    private String prefix = "warehouse/warehouse";

    @Autowired
    private IWWareHouseService wWareHouseService;

    @RequiresPermissions("warehouse:warehouse:view")
    @GetMapping()
    public String warehouse()
    {
        return prefix + "/warehouse";
    }

    /**
     * 查询仓库信息主表列表
     */
    @ApiOperation("查询仓库信息主表列表")
    @RequiresPermissions("warehouse:warehouse:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(WWareHouse wWareHouse)
    {
        startPage();
        List<WWareHouse> list = wWareHouseService.selectWWareHouseList(wWareHouse);
        return getDataTable(list);
    }

    /**
     * 导出仓库信息主表列表
     */
    @ApiOperation("导出仓库信息主表列表")
    @RequiresPermissions("warehouse:warehouse:export")
    @Log(title = "仓库信息主表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(WWareHouse wWareHouse)
    {
        List<WWareHouse> list = wWareHouseService.selectWWareHouseList(wWareHouse);
        ExcelUtil<WWareHouse> util = new ExcelUtil<WWareHouse>(WWareHouse.class);
        return util.exportExcel(list, "warehouse");
    }

    /**
     * 新增仓库信息主表
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存仓库信息主表
     */
    @ApiOperation("新增保存仓库信息主表")
    @RequiresPermissions("warehouse:warehouse:add")
    @Log(title = "仓库信息主表", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(WWareHouse wWareHouse)
    {
        wWareHouse.setPersonNameId(ShiroUtils.getUserId());
        wWareHouse.setRecordPersonName(ShiroUtils.getLoginName());
        return toAjax(wWareHouseService.insertWWareHouse(wWareHouse));
    }

    /**
     * 修改仓库信息主表
     */
    @GetMapping("/edit/{wareHouseId}")
    public String edit(@PathVariable("wareHouseId") Long wareHouseId, ModelMap mmap)
    {
        WWareHouse wWareHouse = wWareHouseService.selectWWareHouseById(wareHouseId);
        mmap.put("wWareHouse", wWareHouse);
        return prefix + "/edit";
    }

    /**
     * 修改保存仓库信息主表
     */
    @ApiOperation("修改保存仓库信息主表")
    @RequiresPermissions("warehouse:warehouse:edit")
    @Log(title = "仓库信息主表", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(WWareHouse wWareHouse)
    {
        return toAjax(wWareHouseService.updateWWareHouse(wWareHouse));
    }

    /**
     * 删除仓库信息主表
     */
    @ApiOperation("删除仓库信息主表")
    @RequiresPermissions("warehouse:warehouse:remove")
    @Log(title = "仓库信息主表", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(wWareHouseService.deleteWWareHouseByIds(ids));
    }
}
