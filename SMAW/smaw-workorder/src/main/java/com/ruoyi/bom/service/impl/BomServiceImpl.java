package com.ruoyi.bom.service.impl;

import com.ruoyi.bom.domain.Bom;
import com.ruoyi.bom.mapper.BomMapper;
import com.ruoyi.bom.service.IBomService;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * bom主表Service业务层处理
 *
 * @author Jerry
 * @date 2020-05-27
 */
@Service
public class BomServiceImpl implements IBomService
{
    @Autowired
    private BomMapper bomMapper;

    /**
     * 查询bom主表
     *
     * @param bomId bom主表ID
     * @return bom主表
     */
    @Override
    public Bom selectBomById(Long bomId)
    {
        return bomMapper.selectBomById(bomId);
    }

    /**
     * 查询bom主表列表
     *
     * @param bom bom主表
     * @return bom主表
     */
    @Override
    public List<Bom> selectBomList(Bom bom)
    {
        return bomMapper.selectBomList(bom);
    }

    /**
     * 新增bom主表
     *
     * @param bom bom主表
     * @return 结果
     */
    @Override
    public int insertBom(Bom bom)
    {
        bom.setCreateTime(DateUtils.getNowDate());
        return bomMapper.insertBom(bom);
    }

    /**
     * 修改bom主表
     *
     * @param bom bom主表
     * @return 结果
     */
    @Override
    public int updateBom(Bom bom)
    {
        bom.setUpdateTime(DateUtils.getNowDate());
        return bomMapper.updateBom(bom);
    }

    /**
     * 删除bom主表对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBomByIds(String ids)
    {
        return bomMapper.deleteBomByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除bom主表信息
     *
     * @param bomId bom主表ID
     * @return 结果
     */
    @Override
    public int deleteBomById(Long bomId)
    {
        return bomMapper.deleteBomById(bomId);
    }

    @Override
    public List<Bom> selectAll() {
        return bomMapper.selectAll();
    }
}