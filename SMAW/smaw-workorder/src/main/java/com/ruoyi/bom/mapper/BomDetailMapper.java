package com.ruoyi.bom.mapper;

import com.ruoyi.system.domain.BomDetail;

import java.util.List;

/**
 * bom明细Mapper接口
 *
 * @author Jerry
 * @date 2020-05-27
 */
public interface BomDetailMapper
{
    /**
     * 查询bom明细
     *
     * @param bomDetailId bom明细ID
     * @return bom明细
     */
    public BomDetail selectBomDetailById(Long bomDetailId);

    /**
     * 查询bom明细列表
     *
     * @param bomDetail bom明细
     * @return bom明细集合
     */
    public List<BomDetail> selectBomDetailList(BomDetail bomDetail);

    /**
     * 新增bom明细
     *
     * @param bomDetail bom明细
     * @return 结果
     */
    public int insertBomDetail(BomDetail bomDetail);

    /**
     * 修改bom明细
     *
     * @param bomDetail bom明细
     * @return 结果
     */
    public int updateBomDetail(BomDetail bomDetail);

    /**
     * 删除bom明细
     *
     * @param bomDetailId bom明细ID
     * @return 结果
     */
    public int deleteBomDetailById(Long bomDetailId);

    /**
     * 批量删除bom明细
     *
     * @param bomDetailIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteBomDetailByIds(String[] bomDetailIds);

    public List<BomDetail> selectAll();
}