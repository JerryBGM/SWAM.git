package com.ruoyi.bom.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * bom主表对象 bom
 *
 * @author Jerry
 * @date 2020-05-27
 */
public class Bom extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** bom_id */
    private Long bomId;

    /** bom名称 */
    @Excel(name = "bom名称")
    private String bomName;

    public void setBomId(Long bomId)
    {
        this.bomId = bomId;
    }

    public Long getBomId()
    {
        return bomId;
    }
    public void setBomName(String bomName)
    {
        this.bomName = bomName;
    }

    public String getBomName()
    {
        return bomName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("bomId", getBomId())
                .append("bomName", getBomName())
                .append("createTime", getCreateTime())
                .append("createBy", getCreateBy())
                .append("updateTime", getUpdateTime())
                .append("updateBy", getUpdateBy())
                .toString();
    }
}