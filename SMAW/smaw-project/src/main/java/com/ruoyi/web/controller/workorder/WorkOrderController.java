package com.ruoyi.web.controller.workorder;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.workorder.domain.WorkOrder;
import com.ruoyi.workorder.service.IWorkOrderService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 生产工单Controller
 * 
 * @author Jerry
 * @date 2020-06-03
 */
@Controller
@RequestMapping("/WorkOrder/workorder")
public class WorkOrderController extends BaseController
{
    private String prefix = "WorkOrder/workorder";

    @Autowired
    private IWorkOrderService workOrderService;
    @Autowired
    private ISysUserService sysUserService;

    @RequiresPermissions("WorkOrder:workorder:view")
    @GetMapping()
    public String workorder()
    {
        return prefix + "/workorder";
    }

    /**
     * 查询生产工单列表
     */
    @RequiresPermissions("WorkOrder:workorder:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(WorkOrder workOrder)
    {
        startPage();
        List<WorkOrder> list = workOrderService.selectWorkOrderList(workOrder);
        return getDataTable(list);
    }

    /**
     * 导出生产工单列表
     */
    @RequiresPermissions("WorkOrder:workorder:export")
    @Log(title = "生产工单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(WorkOrder workOrder)
    {
        List<WorkOrder> list = workOrderService.selectWorkOrderList(workOrder);
        ExcelUtil<WorkOrder> util = new ExcelUtil<WorkOrder>(WorkOrder.class);
        return util.exportExcel(list, "workorder");
    }

    /**
     * 新增生产工单
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
        SysUser sysUser = new SysUser();
        mmap.put("user",sysUserService.selectUserList(sysUser));
        return prefix + "/add";
    }

    /**
     * 新增保存生产工单
     */
    @RequiresPermissions("WorkOrder:workorder:add")
    @Log(title = "生产工单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(WorkOrder workOrder)
    {
        workOrder.setCreatePerson(ShiroUtils.getLoginName());
        return toAjax(workOrderService.insertWorkOrder(workOrder));
    }

    /**
     * 修改生产工单
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        WorkOrder workOrder = workOrderService.selectWorkOrderById(id);
        SysUser sysUser = new SysUser();
        mmap.put("workOrder", workOrder);
        mmap.put("user",sysUserService.selectUserList(sysUser));
        return prefix + "/edit";
    }

    /**
     * 修改保存生产工单
     */
    @RequiresPermissions("WorkOrder:workorder:edit")
    @Log(title = "生产工单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(WorkOrder workOrder)
    {
        return toAjax(workOrderService.updateWorkOrder(workOrder));
    }

    /**
     * 删除生产工单
     */
    @RequiresPermissions("WorkOrder:workorder:remove")
    @Log(title = "生产工单", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(workOrderService.deleteWorkOrderByIds(ids));
    }

    @Log(title = "设备数据导入", businessType = BusinessType.IMPORT)
    @RequiresPermissions("system:user:import")
    @PostMapping("/importData")
    @ResponseBody
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<WorkOrder> util = new ExcelUtil<WorkOrder>(WorkOrder.class);
        List<WorkOrder> workOrderList = util.importExcel(file.getInputStream());

        String operName = ShiroUtils.getSysUser().getLoginName();
        String message = workOrderService.importWorkOrder(workOrderList, updateSupport, operName);

        return AjaxResult.success(message);
    }

    @RequiresPermissions("system:user:view")
    @GetMapping("/importTemplate")
    @ResponseBody
    public AjaxResult importTemplate()
    {
        ExcelUtil<WorkOrder> util = new ExcelUtil<WorkOrder>(WorkOrder.class);
        return util.importTemplateExcel("设备数据");
    }
}
