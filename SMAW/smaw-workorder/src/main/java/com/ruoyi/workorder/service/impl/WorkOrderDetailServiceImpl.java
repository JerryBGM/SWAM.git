package com.ruoyi.workorder.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.workorder.mapper.WorkOrderDetailMapper;
import com.ruoyi.workorder.domain.WorkOrderDetail;
import com.ruoyi.workorder.service.IWorkOrderDetailService;
import com.ruoyi.common.core.text.Convert;

/**
 * 工单明细Service业务层处理
 * 
 * @author Jerry
 * @date 2020-06-04
 */
@Service
public class WorkOrderDetailServiceImpl implements IWorkOrderDetailService 
{
    @Autowired
    private WorkOrderDetailMapper workOrderDetailMapper;

    /**
     * 查询工单明细
     * 
     * @param detailId 工单明细ID
     * @return 工单明细
     */
    @Override
    public WorkOrderDetail selectWorkOrderDetailById(Long detailId)
    {
        return workOrderDetailMapper.selectWorkOrderDetailById(detailId);
    }

    /**
     * 查询工单明细列表
     * 
     * @param workOrderDetail 工单明细
     * @return 工单明细
     */
    @Override
    public List<WorkOrderDetail> selectWorkOrderDetailList(WorkOrderDetail workOrderDetail)
    {
        return workOrderDetailMapper.selectWorkOrderDetailList(workOrderDetail);
    }

    /**
     * 新增工单明细
     * 
     * @param workOrderDetail 工单明细
     * @return 结果
     */
    @Override
    public int insertWorkOrderDetail(WorkOrderDetail workOrderDetail)
    {
        workOrderDetail.setCreateTime(DateUtils.getNowDate());
        return workOrderDetailMapper.insertWorkOrderDetail(workOrderDetail);
    }

    /**
     * 修改工单明细
     * 
     * @param workOrderDetail 工单明细
     * @return 结果
     */
    @Override
    public int updateWorkOrderDetail(WorkOrderDetail workOrderDetail)
    {
        return workOrderDetailMapper.updateWorkOrderDetail(workOrderDetail);
    }

    /**
     * 删除工单明细对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteWorkOrderDetailByIds(String ids)
    {
        return workOrderDetailMapper.deleteWorkOrderDetailByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除工单明细信息
     * 
     * @param detailId 工单明细ID
     * @return 结果
     */
    @Override
    public int deleteWorkOrderDetailById(Long detailId)
    {
        return workOrderDetailMapper.deleteWorkOrderDetailById(detailId);
    }
}
