package com.ruoyi.web.controller.warehouse;

import com.ruoyi.warehouse.domain.WWareHouse;
import com.ruoyi.warehouse.service.IWWareHouseService;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
@Api("库存")
@Controller
@RequestMapping("/warehouse/stockIndex")
public class stockController {
    private String prefix = "warehouse/stock";

    @Autowired
    private IWWareHouseService wWareHouseService;

    @RequiresPermissions("warehouse:stockIndex:view")
    @GetMapping()
    public String warehouse(WWareHouse wWareHouse,ModelMap mmap)
    {
        List<WWareHouse> list = wWareHouseService.selectWWareHouseList(wWareHouse);
        mmap.put("detailList",list);
        return prefix + "/stockIndex";
    }

}

