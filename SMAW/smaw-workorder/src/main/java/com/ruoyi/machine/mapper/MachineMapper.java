package com.ruoyi.machine.mapper;

import com.ruoyi.machine.domain.Machine;

import java.util.List;

/**
 * 设备Mapper接口
 * 
 * @author Jerry
 * @date 2020-06-02
 */
public interface MachineMapper 
{
    /**
     * 查询设备
     * 
     * @param machineId 设备ID
     * @return 设备
     */
    public Machine selectMachineById(Long machineId);

    /**
     * 查询设备列表
     * 
     * @param machine 设备
     * @return 设备集合
     */
    public List<Machine> selectMachineList(Machine machine);

    /**
     * 新增设备
     * 
     * @param machine 设备
     * @return 结果
     */
    public int insertMachine(Machine machine);

    /**
     * 修改设备
     * 
     * @param machine 设备
     * @return 结果
     */
    public int updateMachine(Machine machine);

    /**
     * 删除设备
     * 
     * @param machineId 设备ID
     * @return 结果
     */
    public int deleteMachineById(Long machineId);

    /**
     * 批量删除设备
     * 
     * @param machineIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMachineByIds(String[] machineIds);
}
