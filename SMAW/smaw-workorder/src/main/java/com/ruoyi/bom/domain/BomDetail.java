package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * bom明细对象 bom_detail
 *
 * @author ruoyi
 * @date 2020-06-01
 */
public class BomDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long bomDetailId;

    /** 数量 */
    @Excel(name = "数量")
    private Double bomNumber;

    /** $column.columnComment */
    @Excel(name = "数量")
    private String bomName;

    /** bom组成 */
    @Excel(name = "bom组成")
    private String bomPart;

    /** bom原材料类型 */
    @Excel(name = "bom原材料类型")
    private String type;

    public void setBomDetailId(Long bomDetailId)
    {
        this.bomDetailId = bomDetailId;
    }

    public Long getBomDetailId()
    {
        return bomDetailId;
    }
    public void setBomNumber(Double bomNumber)
    {
        this.bomNumber = bomNumber;
    }

    public Double getBomNumber()
    {
        return bomNumber;
    }
    public void setBomName(String bomName)
    {
        this.bomName = bomName;
    }

    public String getBomName()
    {
        return bomName;
    }
    public void setBomPart(String bomPart)
    {
        this.bomPart = bomPart;
    }

    public String getBomPart()
    {
        return bomPart;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("bomDetailId", getBomDetailId())
                .append("bomNumber", getBomNumber())
                .append("bomName", getBomName())
                .append("bomPart", getBomPart())
                .append("createTime", getCreateTime())
                .append("createBy", getCreateBy())
                .append("type", getType())
                .append("updateTime", getUpdateTime())
                .append("updateBy", getUpdateBy())
                .toString();
    }
}