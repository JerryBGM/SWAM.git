package com.ruoyi.warehouse.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 仓库信息主表对象 w_ware_house
 * 
 * @author Jerry
 * @date 2020-04-13
 */
public class WWareHouse extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 仓库id */
    private Long wareHouseId;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String name;

    /** 仓库位置 */
    @Excel(name = "仓库位置")
    private String location;

    /** 录入时间 */
    @Excel(name = "录入时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recordDate;

    /** 录入人名称 */
    @Excel(name = "录入人名称")
    private String recordPersonName;

    /** 员工id */
    @Excel(name = "员工id")
    private Long personNameId;

    public void setWareHouseId(Long wareHouseId) 
    {
        this.wareHouseId = wareHouseId;
    }

    public Long getWareHouseId() 
    {
        return wareHouseId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setLocation(String location) 
    {
        this.location = location;
    }

    public String getLocation() 
    {
        return location;
    }
    public void setRecordDate(Date recordDate) 
    {
        this.recordDate = recordDate;
    }

    public Date getRecordDate() 
    {
        return recordDate;
    }
    public void setRecordPersonName(String recordPersonName) 
    {
        this.recordPersonName = recordPersonName;
    }

    public String getRecordPersonName() 
    {
        return recordPersonName;
    }
    public void setPersonNameId(Long personNameId) 
    {
        this.personNameId = personNameId;
    }

    public Long getPersonNameId() 
    {
        return personNameId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("wareHouseId", getWareHouseId())
            .append("name", getName())
            .append("location", getLocation())
            .append("recordDate", getRecordDate())
            .append("recordPersonName", getRecordPersonName())
            .append("personNameId", getPersonNameId())
            .toString();
    }
}
