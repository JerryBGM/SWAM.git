package com.ruoyi.machine.service;

import com.ruoyi.machine.domain.Machine;
import java.util.List;

/**
 * 设备Service接口
 *
 * @author Jerry
 * @date 2020-06-02
 */
public interface IMachineService
{
    /**
     * 查询设备
     *
     * @param machineId 设备ID
     * @return 设备
     */
    public Machine selectMachineById(Long machineId);

    /**
     * 查询设备列表
     *
     * @param machine 设备
     * @return 设备集合
     */
    public List<Machine> selectMachineList(Machine machine);

    /**
     * 新增设备
     *
     * @param machine 设备
     * @return 结果
     */
    public int insertMachine(Machine machine);

    /**
     * 修改设备
     *
     * @param machine 设备
     * @return 结果
     */
    public int updateMachine(Machine machine);

    /**
     * 批量删除设备
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMachineByIds(String ids);

    /**
     * 删除设备信息
     *
     * @param machineId 设备ID
     * @return 结果
     */
    public int deleteMachineById(Long machineId);

    /**
     * 导入设备数据
     *
     * @param machineList 设备数据
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importMachine(List<Machine> machineList, Boolean isUpdateSupport, String operName);
}
