package com.ruoyi.bom.service.impl;

import com.ruoyi.bom.mapper.BomDetailMapper;
import com.ruoyi.bom.service.IBomDetailService;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.BomDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * bom明细Service业务层处理
 *
 * @author Jerry
 * @date 2020-05-27
 */
@Service
public class BomDetailServiceImpl implements IBomDetailService
{
    @Autowired
    private BomDetailMapper bomDetailMapper;

    /**
     * 查询bom明细
     *
     * @param bomDetailId bom明细ID
     * @return bom明细
     */
    @Override
    public BomDetail selectBomDetailById(Long bomDetailId)
    {
        return bomDetailMapper.selectBomDetailById(bomDetailId);
    }

    /**
     * 查询bom明细列表
     *
     * @param bomDetail bom明细
     * @return bom明细
     */
    @Override
    public List<BomDetail> selectBomDetailList(BomDetail bomDetail)
    {
        return bomDetailMapper.selectBomDetailList(bomDetail);
    }

    /**
     * 新增bom明细
     *
     * @param bomDetail bom明细
     * @return 结果
     */
    @Override
    public int insertBomDetail(BomDetail bomDetail)
    {
        bomDetail.setCreateTime(DateUtils.getNowDate());
        return bomDetailMapper.insertBomDetail(bomDetail);
    }

    /**
     * 修改bom明细
     *
     * @param bomDetail bom明细
     * @return 结果
     */
    @Override
    public int updateBomDetail(BomDetail bomDetail)
    {
        bomDetail.setUpdateTime(DateUtils.getNowDate());
        return bomDetailMapper.updateBomDetail(bomDetail);
    }

    /**
     * 删除bom明细对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBomDetailByIds(String ids)
    {
        return bomDetailMapper.deleteBomDetailByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除bom明细信息
     *
     * @param bomDetailId bom明细ID
     * @return 结果
     */
    @Override
    public int deleteBomDetailById(Long bomDetailId)
    {
        return bomDetailMapper.deleteBomDetailById(bomDetailId);
    }

}