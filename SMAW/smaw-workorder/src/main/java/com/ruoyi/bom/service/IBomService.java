package com.ruoyi.bom.service;

import com.ruoyi.bom.domain.Bom;

import java.util.List;

/**
 * bom主表Service接口
 *
 * @author Jerry
 * @date 2020-05-27
 */
public interface IBomService
{
    /**
     * 查询bom主表
     *
     * @param bomId bom主表ID
     * @return bom主表
     */
    public Bom selectBomById(Long bomId);

    /**
     * 查询bom主表列表
     *
     * @param bom bom主表
     * @return bom主表集合
     */
    public List<Bom> selectBomList(Bom bom);

    /**
     * 新增bom主表
     *
     * @param bom bom主表
     * @return 结果
     */
    public int insertBom(Bom bom);

    /**
     * 修改bom主表
     *
     * @param bom bom主表
     * @return 结果
     */
    public int updateBom(Bom bom);

    /**
     * 批量删除bom主表
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBomByIds(String ids);

    /**
     * 删除bom主表信息
     *
     * @param bomId bom主表ID
     * @return 结果
     */
    public int deleteBomById(Long bomId);

    public List<Bom> selectAll();
}