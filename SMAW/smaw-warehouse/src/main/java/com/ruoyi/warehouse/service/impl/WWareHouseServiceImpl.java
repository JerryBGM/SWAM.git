package com.ruoyi.warehouse.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.warehouse.mapper.WWareHouseMapper;
import com.ruoyi.warehouse.domain.WWareHouse;
import com.ruoyi.warehouse.service.IWWareHouseService;
import com.ruoyi.common.core.text.Convert;

/**
 * 仓库信息主表Service业务层处理
 * 
 * @author Jerry
 * @date 2020-04-13
 */
@Service
public class WWareHouseServiceImpl implements IWWareHouseService 
{
    @Autowired
    private WWareHouseMapper wWareHouseMapper;

    /**
     * 查询仓库信息主表
     * 
     * @param wareHouseId 仓库信息主表ID
     * @return 仓库信息主表
     */
    @Override
    public WWareHouse selectWWareHouseById(Long wareHouseId)
    {
        return wWareHouseMapper.selectWWareHouseById(wareHouseId);
    }

    /**
     * 查询仓库信息主表列表
     * 
     * @param wWareHouse 仓库信息主表
     * @return 仓库信息主表
     */
    @Override
    public List<WWareHouse> selectWWareHouseList(WWareHouse wWareHouse)
    {
        return wWareHouseMapper.selectWWareHouseList(wWareHouse);
    }

    /**
     * 新增仓库信息主表
     * 
     * @param wWareHouse 仓库信息主表
     * @return 结果
     */
    @Override
    public int insertWWareHouse(WWareHouse wWareHouse)
    {
        return wWareHouseMapper.insertWWareHouse(wWareHouse);
    }

    /**
     * 修改仓库信息主表
     * 
     * @param wWareHouse 仓库信息主表
     * @return 结果
     */
    @Override
    public int updateWWareHouse(WWareHouse wWareHouse)
    {
        return wWareHouseMapper.updateWWareHouse(wWareHouse);
    }

    /**
     * 删除仓库信息主表对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteWWareHouseByIds(String ids)
    {
        return wWareHouseMapper.deleteWWareHouseByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除仓库信息主表信息
     * 
     * @param wareHouseId 仓库信息主表ID
     * @return 结果
     */
    @Override
    public int deleteWWareHouseById(Long wareHouseId)
    {
        return wWareHouseMapper.deleteWWareHouseById(wareHouseId);
    }
}
