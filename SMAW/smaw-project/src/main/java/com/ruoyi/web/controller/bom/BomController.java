package com.ruoyi.web.controller.bom;

import com.ruoyi.bom.domain.Bom;
import com.ruoyi.bom.service.IBomService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * bom主表Controller
 *
 * @author Jerry
 * @date 2020-05-27
 */
@Controller
@RequestMapping("/bom/bom")
public class BomController extends BaseController
{
    private String prefix = "bom/bom";

    @Autowired
    private IBomService bomService;

    @RequiresPermissions("bom:bom:view")
    @GetMapping()
    public String bom()
    {
        return prefix + "/bom";
    }

    /**
     * 查询bom主表列表
     */
    @RequiresPermissions("bom:bom:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Bom bom)
    {
        startPage();
        List<Bom> list = bomService.selectBomList(bom);
        return getDataTable(list);
    }

    /**
     * 导出bom主表列表
     */
    @RequiresPermissions("bom:bom:export")
    @Log(title = "bom主表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Bom bom)
    {
        List<Bom> list = bomService.selectBomList(bom);
        ExcelUtil<Bom> util = new ExcelUtil<Bom>(Bom.class);
        return util.exportExcel(list, "bom");
    }

    /**
     * 新增bom主表
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存bom主表
     */
    @RequiresPermissions("bom:bom:add")
    @Log(title = "bom主表", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Bom bom)
    {
        bom.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(bomService.insertBom(bom));
    }

    /**
     * 修改bom主表
     */
    @GetMapping("/edit/{bomId}")
    public String edit(@PathVariable("bomId") Long bomId, ModelMap mmap)
    {
        Bom bom = bomService.selectBomById(bomId);
        mmap.put("bom", bom);
        return prefix + "/edit";
    }

    /**
     * 修改保存bom主表
     */
    @RequiresPermissions("bom:bom:edit")
    @Log(title = "bom主表", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Bom bom)
    {
        bom.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(bomService.updateBom(bom));
    }

    /**
     * 删除bom主表
     */
    @RequiresPermissions("bom:bom:remove")
    @Log(title = "bom主表", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(bomService.deleteBomByIds(ids));
    }
}