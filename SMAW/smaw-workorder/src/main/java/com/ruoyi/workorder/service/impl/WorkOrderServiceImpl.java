package com.ruoyi.workorder.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.workorder.domain.WorkOrder;
import com.ruoyi.workorder.mapper.WorkOrderMapper;
import com.ruoyi.workorder.service.IWorkOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 生产工单Service业务层处理
 * 
 * @author Jerry
 * @date 2020-06-03
 */
@Service
public class WorkOrderServiceImpl implements IWorkOrderService 
{
    private static final Logger log = LoggerFactory.getLogger(WorkOrderServiceImpl.class);

    @Autowired
    private WorkOrderMapper workOrderMapper;

    /**
     * 查询生产工单
     * 
     * @param id 生产工单ID
     * @return 生产工单
     */
    @Override
    public WorkOrder selectWorkOrderById(Long id)
    {
        return workOrderMapper.selectWorkOrderById(id);
    }

    /**
     * 查询生产工单列表
     * 
     * @param workOrder 生产工单
     * @return 生产工单
     */
    @Override
    public List<WorkOrder> selectWorkOrderList(WorkOrder workOrder)
    {
        return workOrderMapper.selectWorkOrderList(workOrder);
    }

    /**
     * 新增生产工单
     * 
     * @param workOrder 生产工单
     * @return 结果
     */
    @Override
    public int insertWorkOrder(WorkOrder workOrder)
    {
        workOrder.setCreateTime(DateUtils.getNowDate());
        return workOrderMapper.insertWorkOrder(workOrder);
    }

    /**
     * 修改生产工单
     * 
     * @param workOrder 生产工单
     * @return 结果
     */
    @Override
    public int updateWorkOrder(WorkOrder workOrder)
    {
        return workOrderMapper.updateWorkOrder(workOrder);
    }

    /**
     * 删除生产工单对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteWorkOrderByIds(String ids)
    {
        return workOrderMapper.deleteWorkOrderByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除生产工单信息
     * 
     * @param id 生产工单ID
     * @return 结果
     */
    @Override
    public int deleteWorkOrderById(Long id)
    {
        return workOrderMapper.deleteWorkOrderById(id);
    }

    @Override
    public String importWorkOrder(List<WorkOrder> workOrderList, Boolean isUpdateSupport, String operName) {
        if (StringUtils.isNull(workOrderList) || workOrderList.size() == 0)
        {
            throw new BusinessException("导入数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (WorkOrder w : workOrderList){
            try {
                WorkOrder workOrder = new WorkOrder();
                workOrder.setWorkOrder(w.getWorkOrder());
                List<WorkOrder> workOrders = workOrderMapper.selectWorkOrderList(workOrder);
                if(workOrders.size()>0){
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、设备 " + w.getWorkOrder()+ " 已存在");
                }
                w.setCreateBy(operName);
                this.insertWorkOrder(w);
                successNum++;
                successMsg.append("<br/>" + successNum + "、账号 " + w.getWorkOrder() + " 导入成功");
            }catch (Exception e){
                String msg = "";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
            if (failureNum > 0)
            {
                failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
                throw new BusinessException(failureMsg.toString());
            }
            else
            {
                successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
            }
        }
        return successMsg.toString();
    }
}
