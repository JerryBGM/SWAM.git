package com.ruoyi.warehouse.service;

import com.ruoyi.warehouse.domain.WWarehouseReceipt;
import java.util.List;

/**
 * 仓库类单据Service接口
 * 
 * @author Jerry
 * @date 2020-04-17
 */
public interface IWWarehouseReceiptService 
{
    /**
     * 查询仓库类单据
     * 
     * @param wReceiptId 仓库类单据ID
     * @return 仓库类单据
     */
    public WWarehouseReceipt selectWWarehouseReceiptById(Long wReceiptId);

    /**
     * 查询仓库类单据列表
     * 
     * @param wWarehouseReceipt 仓库类单据
     * @return 仓库类单据集合
     */
    public List<WWarehouseReceipt> selectWWarehouseReceiptList(WWarehouseReceipt wWarehouseReceipt);

    /**
     * 新增仓库类单据
     * 
     * @param wWarehouseReceipt 仓库类单据
     * @return 结果
     */
    public int insertWWarehouseReceipt(WWarehouseReceipt wWarehouseReceipt);

    /**
     * 修改仓库类单据
     * 
     * @param wWarehouseReceipt 仓库类单据
     * @return 结果
     */
    public int updateWWarehouseReceipt(WWarehouseReceipt wWarehouseReceipt);

    /**
     * 批量删除仓库类单据
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWWarehouseReceiptByIds(String ids);

    /**
     * 删除仓库类单据信息
     * 
     * @param wReceiptId 仓库类单据ID
     * @return 结果
     */
    public int deleteWWarehouseReceiptById(Long wReceiptId);
}
