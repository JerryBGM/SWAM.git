package com.ruoyi.web.controller.workorder;

import com.ruoyi.bom.domain.Bom;
import com.ruoyi.bom.service.IBomService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.machine.domain.Machine;
import com.ruoyi.machine.service.IMachineService;
import com.ruoyi.workorder.domain.WorkOrder;
import com.ruoyi.workorder.domain.WorkOrderDetail;
import com.ruoyi.workorder.service.IWorkOrderDetailService;
import com.ruoyi.workorder.service.IWorkOrderService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 工单明细Controller
 * 
 * @author Jerry
 * @date 2020-06-04
 */
@Controller
@RequestMapping("/WorkOrder/detail")
public class WorkOrderDetailController extends BaseController
{
    private String prefix = "WorkOrder/detail";

    @Autowired
    private IWorkOrderDetailService workOrderDetailService;
    @Autowired
    private IWorkOrderService workOrderService;
    @Autowired
    private IMachineService machineService;
    @Autowired
    private IBomService bomService;

    @RequiresPermissions("workorder:detail:view")
    @GetMapping()
    public String detail()
    {
        return prefix + "/detail";
    }

    /**
     * 查询工单明细列表
     */
    @RequiresPermissions("workorder:detail:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(WorkOrderDetail workOrderDetail)
    {
        startPage();
        List<WorkOrderDetail> list = workOrderDetailService.selectWorkOrderDetailList(workOrderDetail);
        return getDataTable(list);
    }

    /**
     * 导出工单明细列表
     */
    @RequiresPermissions("workorder:detail:export")
    @Log(title = "工单明细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(WorkOrderDetail workOrderDetail)
    {
        List<WorkOrderDetail> list = workOrderDetailService.selectWorkOrderDetailList(workOrderDetail);
        ExcelUtil<WorkOrderDetail> util = new ExcelUtil<WorkOrderDetail>(WorkOrderDetail.class);
        return util.exportExcel(list, "detail");
    }

    /**
     * 新增工单明细
     */
    @GetMapping("/add/{workOrderId}")
    public String add(@PathVariable("workOrderId") String workOrderId,ModelMap mmap)
    {
        Machine machine = new Machine();
        Bom bom = new Bom();
        mmap.put("machine",machineService.selectMachineList(machine));
        mmap.put("bom",bomService.selectBomList(bom));
        mmap.put("workOrder",workOrderService.selectWorkOrderById(Long.valueOf(workOrderId)));
        return prefix + "/add";
    }

    /**
     * 新增保存工单明细
     */
    @RequiresPermissions("workorder:detail:add")
    @Log(title = "工单明细", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(WorkOrderDetail workOrderDetail)
    {
        workOrderDetail.setStatus("1");
        return toAjax(workOrderDetailService.insertWorkOrderDetail(workOrderDetail));
    }

    /**
     * 修改工单明细
     */
    @GetMapping("/edit/{detailId}")
    public String edit(@PathVariable("detailId") Long detailId, ModelMap mmap)
    {
        WorkOrderDetail workOrderDetail = workOrderDetailService.selectWorkOrderDetailById(detailId);
        mmap.put("workOrderDetail", workOrderDetail);
        return prefix + "/edit";
    }

    /**
     * 修改保存工单明细
     */
    @RequiresPermissions("workorder:detail:edit")
    @Log(title = "工单明细", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(WorkOrderDetail workOrderDetail)
    {
        return toAjax(workOrderDetailService.updateWorkOrderDetail(workOrderDetail));
    }

    /**
     * 删除工单明细
     */
    @RequiresPermissions("workorder:detail:remove")
    @Log(title = "工单明细", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(workOrderDetailService.deleteWorkOrderDetailByIds(ids));
    }

    /**
     * 查询详细
     */
    @RequiresPermissions("workorder:detail:list")
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long id,ModelMap map)
    {
        WorkOrder workOrder = new WorkOrder();
        map.put("workOrderId",workOrderService.selectWorkOrderById(id));
        map.put("workOrderList",workOrderService.selectWorkOrderList(workOrder));
        return "WorkOrder/detail/detail";
    }

    /**
     * 修改保存工单明细
     */
    @RequiresPermissions("workorder:detail:edit")
    @Log(title = "工单明细", businessType = BusinessType.UPDATE)
    @PostMapping("/changeStatus")
    @ResponseBody
    public AjaxResult changeStatus(WorkOrderDetail workOrderDetail)
    {

        return toAjax(workOrderDetailService.updateWorkOrderDetail(workOrderDetail));
    }
}
