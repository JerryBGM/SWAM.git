package com.ruoyi.bom.mapper;

import com.ruoyi.bom.domain.Bom;
import java.util.List;

/**
 * bom主表Mapper接口
 *
 * @author Jerry
 * @date 2020-05-27
 */
public interface BomMapper
{
    /**
     * 查询bom主表
     *
     * @param bomId bom主表ID
     * @return bom主表
     */
    public Bom selectBomById(Long bomId);

    /**
     * 查询bom主表列表
     *
     * @param bom bom主表
     * @return bom主表集合
     */
    public List<Bom> selectBomList(Bom bom);

    /**
     * 新增bom主表
     *
     * @param bom bom主表
     * @return 结果
     */
    public int insertBom(Bom bom);

    /**
     * 修改bom主表
     *
     * @param bom bom主表
     * @return 结果
     */
    public int updateBom(Bom bom);

    /**
     * 删除bom主表
     *
     * @param bomId bom主表ID
     * @return 结果
     */
    public int deleteBomById(Long bomId);

    /**
     * 批量删除bom主表
     *
     * @param bomIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteBomByIds(String[] bomIds);

    public List<Bom> selectAll();
}