package com.ruoyi.web.controller.warehouse;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.warehouse.domain.WWarehouseReceipt;
import com.ruoyi.warehouse.service.IWWarehouseReceiptService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 仓库类单据Controller
 * 
 * @author Jerry
 * @date 2020-04-17
 */
@Controller
@RequestMapping("/receipt/receipt")
public class WWarehouseReceiptController extends BaseController
{
    private String prefix = "receipt/receipt";

    @Autowired
    private IWWarehouseReceiptService wWarehouseReceiptService;

    @RequiresPermissions("receipt:receipt:view")
    @GetMapping()
    public String receipt()
    {
        return prefix + "/receipt";
    }

    /**
     * 查询仓库类单据列表
     */
    @RequiresPermissions("receipt:receipt:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(WWarehouseReceipt wWarehouseReceipt)
    {
        startPage();
        List<WWarehouseReceipt> list = wWarehouseReceiptService.selectWWarehouseReceiptList(wWarehouseReceipt);
        return getDataTable(list);
    }

    /**
     * 导出仓库类单据列表
     */
    @RequiresPermissions("receipt:receipt:export")
    @Log(title = "仓库类单据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(WWarehouseReceipt wWarehouseReceipt)
    {
        List<WWarehouseReceipt> list = wWarehouseReceiptService.selectWWarehouseReceiptList(wWarehouseReceipt);
        ExcelUtil<WWarehouseReceipt> util = new ExcelUtil<WWarehouseReceipt>(WWarehouseReceipt.class);
        return util.exportExcel(list, "receipt");
    }

    /**
     * 新增仓库类单据
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存仓库类单据
     */
    @RequiresPermissions("receipt:receipt:add")
    @Log(title = "仓库类单据", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(WWarehouseReceipt wWarehouseReceipt)
    {
        wWarehouseReceipt.setwCreatePersonId(ShiroUtils.getUserId());
        wWarehouseReceipt.setwCreatePersonName(ShiroUtils.getLoginName());
        return toAjax(wWarehouseReceiptService.insertWWarehouseReceipt(wWarehouseReceipt));
    }

    /**
     * 修改仓库类单据
     */
    @GetMapping("/edit/{wReceiptId}")
    public String edit(@PathVariable("wReceiptId") Long wReceiptId, ModelMap mmap)
    {
        WWarehouseReceipt wWarehouseReceipt = wWarehouseReceiptService.selectWWarehouseReceiptById(wReceiptId);
        mmap.put("wWarehouseReceipt", wWarehouseReceipt);
        return prefix + "/edit";
    }

    /**
     * 修改保存仓库类单据
     */
    @RequiresPermissions("receipt:receipt:edit")
    @Log(title = "仓库类单据", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(WWarehouseReceipt wWarehouseReceipt)
    {
        return toAjax(wWarehouseReceiptService.updateWWarehouseReceipt(wWarehouseReceipt));
    }

    /**
     * 审核保存仓库类单据
     */
    @RequiresPermissions("receipt:receipt:check")
    @Log(title = "仓库类单据", businessType = BusinessType.UPDATE)
    @PostMapping("/check")
    @ResponseBody
    public AjaxResult check(Long id,String status)
    {
        WWarehouseReceipt wWarehouseReceipt = new WWarehouseReceipt();
        wWarehouseReceipt.setwReceiptId(id);
        wWarehouseReceipt.setIsCheck(status);
        return toAjax(wWarehouseReceiptService.updateWWarehouseReceipt(wWarehouseReceipt));
    }

    /**
     * 删除仓库类单据
     */
    @RequiresPermissions("receipt:receipt:remove")
    @Log(title = "仓库类单据", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(wWarehouseReceiptService.deleteWWarehouseReceiptByIds(ids));
    }
}
