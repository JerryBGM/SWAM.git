package com.ruoyi.warehouse.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.warehouse.domain.WWareStock;
import com.ruoyi.warehouse.mapper.WWareStockMapper;
import com.ruoyi.warehouse.service.IWWareStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 库存Service业务层处理
 * 
 * @author Jerry
 * @date 2020-04-14
 */
@Service
public class WWareStockServiceImpl implements IWWareStockService 
{
    @Autowired
    private WWareStockMapper wWareStockMapper;

    /**
     * 查询库存
     * 
     * @param stockId 库存ID
     * @return 库存
     */
    @Override
    public WWareStock selectWWareStockById(Long stockId)
    {
        return wWareStockMapper.selectWWareStockById(stockId);
    }

    /**
     * 查询库存列表
     * 
     * @param wWareStock 库存
     * @return 库存
     */
    @Override
    public List<WWareStock> selectWWareStockList(WWareStock wWareStock)
    {
        return wWareStockMapper.selectWWareStockList(wWareStock);
    }

    /**
     * 新增库存
     * 
     * @param wWareStock 库存
     * @return 结果
     */
    @Override
    public int insertWWareStock(WWareStock wWareStock)
    {
        return wWareStockMapper.insertWWareStock(wWareStock);
    }

    /**
     * 修改库存
     * 
     * @param wWareStock 库存
     * @return 结果
     */
    @Override
    public int updateWWareStock(WWareStock wWareStock)
    {
        return wWareStockMapper.updateWWareStock(wWareStock);
    }

    /**
     * 删除库存对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteWWareStockByIds(String ids)
    {
        return wWareStockMapper.deleteWWareStockByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除库存信息
     * 
     * @param stockId 库存ID
     * @return 结果
     */
    @Override
    public int deleteWWareStockById(Long stockId)
    {
        return wWareStockMapper.deleteWWareStockById(stockId);
    }

    /**
     *查找库存对象
     *
     * @param stockHouseId 仓库id
     * @return 结果
     */
    public int queryWWareStockBystockHouseId(Long stockHouseId)
    {
        return wWareStockMapper.queryWWareStockBystockHouseId(stockHouseId);
    }
}
